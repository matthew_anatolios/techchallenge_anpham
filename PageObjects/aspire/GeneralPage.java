package aspire;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import constant.Constant;

public class GeneralPage {

	// define Locators
	private final By labelPageTitle = By.xpath("//div[@class='aspire-header__titles']//div[contains(@class,'h4')]");
	private final By divProcessBarComplete = By
			.xpath("//div[contains(@style,'transform: translate3d(0%, -200%, 0px)')]");

	// get Elements
	protected WebElement get_LabelPageTitle() {
		return Constant.WEBDRIVER.findElement(labelPageTitle);
	}

	protected WebElement get_divProcessBarComplete() {
		return Constant.WEBDRIVER.findElement(divProcessBarComplete);
	}

	// Methods
	/**
	 * To check whether element exists or not
	 * 
	 * @param element
	 * @return
	 */
	public boolean isElementExists(By element) {
		return Constant.WEBDRIVER.findElement(element).isDisplayed();
	}

	/**
	 * To check whether element enabled or not
	 * 
	 * @param element
	 * @return
	 */
	public boolean isElementEnabled(By element) {
		return Constant.WEBDRIVER.findElement(element).isEnabled();
	}

	/**
	 * To check whether element enabled or not
	 * 
	 * @param element
	 * @return
	 */
	public boolean isElementSelected(By element) {
		return Constant.WEBDRIVER.findElement(element).isSelected();
	}

	/**
	 * To enter a string or chars into element
	 * 
	 * @param element
	 * @return
	 */
	public void enter(By element, String keys) {
		Constant.WEBDRIVER.findElement(element).sendKeys(keys);
		;
	}
	
	/**
	 * To wait until the loading is completed
	 * 
	 * @param seconds
	 */
	public void waitForLoadingProcess(int seconds) {
		WebDriverWait wait = new WebDriverWait(Constant.WEBDRIVER, seconds);
		wait.until(ExpectedConditions.visibilityOf(get_divProcessBarComplete()));
	}

	/**
	 * To wait until the element is available
	 * 
	 * @param seconds, element
	 */
	public void waitForElement(int seconds, WebElement element) {
		WebDriverWait wait = new WebDriverWait(Constant.WEBDRIVER, seconds);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	/**
	 * To wait until the element is available by locator
	 * 
	 * @param seconds, elementLocator
	 */
	public void waitForElement(int seconds, By elementLocator) {
		WebDriverWait wait = new WebDriverWait(Constant.WEBDRIVER, seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
	}

}
