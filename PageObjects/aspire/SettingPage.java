package aspire;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import constant.Constant;

public class SettingPage extends GeneralPage {

	public SettingPage() {
		// TODO Auto-generated constructor stub
	}

	// Locators
	private final By buttonUserAndAccess = By.xpath("//div[@class='flex items-center' and .//img[contains(@src,'key')]]");

	// get Elements
	protected WebElement get_buttonUserAndAccess() {
		return Constant.WEBDRIVER.findElement(buttonUserAndAccess);
	}
	
	//Methods
	public UserAndAccessPage goToUserAndAccessPage() {
		get_buttonUserAndAccess().click();
		return new UserAndAccessPage();
		
	}

}
