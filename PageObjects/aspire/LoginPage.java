package aspire;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Select;

import constant.Constant;

public class LoginPage extends GeneralPage {
	// Locators
	private final By selectCountry = By.xpath("//div[./i[contains(@class,\"select__dropdown\")]]");
	private final By linkRegister = By.xpath("//a[contains(@class,\"register-link\")]");
	private final By buttonLogin = By.xpath("//button[.//span[text()=\"Login\"]]");
	private final By inputOTP = By.xpath("//input[@data-cy=\"password-input-password\"]");
	private final By inputPhoneNum = By.xpath("//input[@data-cy=\"register-person-phone\"]");

	// get Elements
	protected WebElement get_selectCountry() {
		return Constant.WEBDRIVER.findElement(selectCountry);
	}

	protected WebElement get_linkRegister() {
		return Constant.WEBDRIVER.findElement(linkRegister);
	}

	protected WebElement get_buttonLogin() {
		return Constant.WEBDRIVER.findElement(buttonLogin);
	}

	protected WebElement get_inputOTP() {
		return Constant.WEBDRIVER.findElement(inputOTP);
	}

	protected WebElement get_inputPhoneNum() {
		return Constant.WEBDRIVER.findElement(inputPhoneNum);
	}

	// Methods
	
	public LoginPage open() {
		Constant.WEBDRIVER.get(Constant.UAT_URL);
		return this;
	}
	
	public void selectNationalCode_obsolete(String nation) {
		Select select_nationalCode = new Select(get_selectCountry());
		select_nationalCode.selectByVisibleText(nation);
	}
	
	public void selectNationalCode(String nation) {
		get_selectCountry().submit();
		get_selectCountry().sendKeys(nation);
		get_selectCountry().sendKeys("{enter}");
	} 
	
	public HomePage loginByPhoneNum(String nation, String phoneNum, String otp) {
		selectNationalCode(nation);
		get_inputPhoneNum().sendKeys(phoneNum);
		get_buttonLogin().submit();
		get_inputOTP().sendKeys(otp);
		return new HomePage();
	}

}
