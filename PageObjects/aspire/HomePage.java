package aspire;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import constant.Constant;

public class HomePage extends GeneralPage {

	public HomePage() {
		// TODO Auto-generated constructor stub
	}

	// Locators
	private final By linkProfile = By.xpath("//a[.//div[text()='Profile']]");

	// get Elements
	protected WebElement get_linkProfile() {
		return Constant.WEBDRIVER.findElement(linkProfile);
	}
	
	//Methods
	
	public SettingPage goToSettingPage() {
		get_linkProfile().click();
		return new SettingPage();
	}

}
