package aspire;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import constant.Constant;

public class UserAndAccessPage extends GeneralPage {

	public UserAndAccessPage() {
		// TODO Auto-generated constructor stub
	}

	// Locators
	private final By iconInviteNewUser = By.xpath("//div[./img[contains(@src,'plus')]]");
	private final By inputFullLegalName = By.xpath("//input[@type='text']");
	private final By inputEmail = By.xpath("//input[@type='email']");
	private final By inputRadioDirector = By.xpath("//div[@role='radio' and .//div[text()='Director']]");
	private final By inputRadioEmployee = By.xpath("//div[@role='radio' and .//div[text()='Employee']]");
	private final By inputRadioOther = By.xpath("//div[@role='radio' and .//div[text()='Other']]");

	// get Elements
	protected WebElement get_iconInviteNewUser() {
		return Constant.WEBDRIVER.findElement(iconInviteNewUser);
	}

	protected WebElement get_inputFullLegalName() {
		return Constant.WEBDRIVER.findElement(inputFullLegalName);
	}

	protected WebElement get_inputEmail() {
		return Constant.WEBDRIVER.findElement(inputEmail);
	}

	protected WebElement get_inputRadioDirector() {
		return Constant.WEBDRIVER.findElement(inputRadioDirector);
	}

	protected WebElement get_inputRadioEmployee() {
		return Constant.WEBDRIVER.findElement(inputRadioEmployee);
	}

	protected WebElement get_inputRadioOther() {
		return Constant.WEBDRIVER.findElement(inputRadioOther);
	}

	// Methods

}
