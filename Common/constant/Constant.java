package constant;
import org.openqa.selenium.WebDriver;

public class Constant {
	public static WebDriver WEBDRIVER;
	public static final String UAT_URL = "https://feature-qa.customer-frontend.staging.aspireapp.com/sg/";
	public static final String PHONE_NUMBER = "989677728";
	public static final String OTP_CODE = "135790";
	
	public static final int SHORT_TIME = 10;
	public static final int MEDIUM_TIME = 30;
	public static final int LONG_TIME = 60;

}
