package constant;

public enum Roles {
	DIRECTOR("Director"), EMPLOYEE("Employee"), OTHER("Other");

	private final String userRole;

	Roles(final String role) {
		// TODO Auto-generated constructor stub
		userRole = role;
	}

	public String getUserRole() {
		return userRole;
	}
}
