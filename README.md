# com.anpham.test
## DataObjects
- Contains test data of Data driven.
## PageObjects
- Each page is corresponding to a Class.
- Each class has: Locators, Elements, Methods (particular methods related to the object).
## Common
- Contains common figures of Testcases: URL, Enum of roles of user etc.
- Contains common methods related to system for running testcases.
## Testcases
- Contains testcases only.
- Each class is one test suite which contains at least one test case.
- Run test cases via TestNG.
# Environment
- Driver: Chrome
- TestNG
- Selenium for Java