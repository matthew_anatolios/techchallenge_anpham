package aspire;

import org.testng.Assert;
import org.testng.annotations.Test;

import constant.Constant;

public class UserAndAccessTest {

	@Test
	public void TC02() {
		// Summary
		System.out.println("TC02: User can navigate to User and Access page after logging in successfully");

		// Variables
		String expectedMessage = "User and Access";

		// ===Steps===
		// 1. Navigate to Aspire Web site
		// 2. Select the National Code
		// 3. Enter valid phone number
		// 4. Click on "Login" button
		// 5. When OTP page displays, enter OTP

		String userPageTitle = new LoginPage().open()
				.loginByPhoneNum("US", Constant.PHONE_NUMBER, Constant.OTP_CODE)
				.goToSettingPage()
				.goToUserAndAccessPage().get_LabelPageTitle().getText();
		Assert.assertEquals(userPageTitle.contains(expectedMessage), true, "Navigate to User and Access page failed!");
	}

}
