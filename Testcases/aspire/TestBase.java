package aspire;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import constant.Constant;
import constant.Ultilities;

public class TestBase {

	protected HomePage homePage;
	protected Account account;

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Pre-condition");

		initChromeDriver();
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("Post-condition");
		Constant.WEBDRIVER.quit();
	}

	protected void initChromeDriver() {
		System.setProperty("webdriver.chrome.driver", Ultilities.getProjectPath() + "\\Executables\\chromedriver.exe");
		Constant.WEBDRIVER = new ChromeDriver();
		Constant.WEBDRIVER.manage().window().maximize();
	}
}
