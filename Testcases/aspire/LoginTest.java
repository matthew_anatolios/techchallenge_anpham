package aspire;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import constant.Constant;

public class LoginTest extends TestBase{

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Pre-condition");

		initChromeDriver();
	}
	
	@Test
	public void TC01() {
		// Summary
		System.out.println("TC01: User can log into Aspire with valid phone number and OTP");

		// Variables
		String expectedMessage = "Welcome";

		// ===Steps===
		// 1. Navigate to Aspire Web site
		// 2. Select the National Code
		// 3. Enter valid phone number
		// 4. Click on "Login" button
		// 5. When OTP page displays, enter OTP

		LoginPage loginPage = new LoginPage();
		loginPage.open();
		try {
			Thread.sleep(Constant.SHORT_TIME*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//loginPage.open().waitForLoadingProcess(Constant.LONG_TIME);
		String userPageTile = loginPage.loginByPhoneNum("US", Constant.PHONE_NUMBER, Constant.OTP_CODE).get_LabelPageTitle().getText();
		
		// VP: System redirect users to User page automatically
		Assert.assertEquals(userPageTile.contains(expectedMessage), true, "Login Failed!");
	}
}
